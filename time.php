<?php

echo countOffHours('2014-11-28 11:00', '2014-11-29 23:00');
//echo calculateMoney('2014-11-28 11:00', '2014-11-29 16:00');

function calculateMoney($dateTimeIn, $dateTimeOut) {

	//11:00 - 16:00 (+1)
	$hours = getTimeDifference($dateTimeIn, $dateTimeOut); //29 = 24 + 5

	if ($hours < 1) {
		return 0;
	}
	if ($hours < 4) {
		return 10;
	}
	
	$offHour = countOffHours($dateTimeIn, $dateTimeOut); //return 3.5

	$onHour = $hours - $offHour - 4; // 29 - 4 - 3.5 = 21.5
	echo 'onHour = '.$onHour.'<br>';
	echo 'offHour = '.$offHour.'<br>';
	
	return 10 + ceil($onHour) * 20 +  ceil($offHour) * 250;

}

function getTimeDifference($strDateIn, $strDateOut)
{
	$dateTimeIn = new DateTime($strDateIn);
	$dateTimeOut = new DateTime($strDateOut);
	
	$interval = $dateTimeIn->diff($dateTimeOut);
	$hour = $interval->format('%h') + ($interval->days*24);
	$min = $interval->format('%i');

	return ($min > 0) ? $hour + 1 : $hour;  
}

function countOffHours($dateTimeIn, $dateTimeOut) 
{
	$dateIn = strtotime($dateTimeIn);
	$dateInDate = date('d', $dateIn);
	$dateInMonth = date('m', $dateIn);
	$dateInYear = date('Y', $dateIn);
	
	echo $dateInDate.'<br>';
	echo $dateInMonth.'<br>';
	echo $dateInYear.'<br>';
	
	//if (strtotime($dateTimeOut) > strtotime("Tomorrow 6am") ) { 
	if (strtotime($dateTimeOut) > mktime(6, 0, 0, $dateInMonth, $dateInDate + 1, $dateInYear) ) { 
		return 3.5;
	} else {
		return 0;
	}
}