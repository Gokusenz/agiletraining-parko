<?php
/**
 * A class to handle secure encryption and decryption of arbitrary data
 *
 * Note that this is not just straight encryption.  It also has a few other
 *  features in it to make the encrypted data far more secure.  Note that any
 *  other implementations used to decrypt data will have to do the same exact
 *  operations.
 *
 * Security Benefits:
 *
 * - Uses Key stretching
 * - Hides the Initialization Vector
 * - Does HMAC verification of source data
 * Example : Encryption::Password()
 */
class Encryption {

    /*
     * Do not change algorithm
     */

    private static $key = "GOKU";

    public static function Password($string) {
        return hash_hmac('SHA1', $string, self::$key);
    }

    public static function Genpassword() {
        return substr(md5(mktime()), 0,5);
    }

}
