<?php

class Client {

    public static function getClient() {
        $client = array();
        $client['ip_address'] = self::getClientAddress();
        $client['browser'] = self::getBrowser();
        $client['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        return $client;
    }

    public static function getClientAddress() {
        $clientAddress = '';
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            if(!empty($clientAddress)) {
                $clientAddress .= ',';
            }
            $clientAddress .= str_ireplace(' ', '', $_SERVER['HTTP_X_FORWARDED_FOR']);
        }
        if(isset($_SERVER['REMOTE_ADDR'])) {
            if(!empty($clientAddress)) {
                $clientAddress .= ',';
            }
            $clientAddress .= str_ireplace(' ', '', $_SERVER['REMOTE_ADDR']);
        }
        return $clientAddress;
    }

    protected static function getBrowser() {

        if(self::chkBrowser("MSIE 9")) {
            return "IE 9";
        }elseif(self::chkBrowser("MSIE 8")) {
            return "IE 8";
        }elseif(self::chkBrowser("MSIE 7")){
            return "IE 7";
        }elseif(self::chkBrowser("MSIE 6")){
            return "IE 6";
        }elseif(self::chkBrowser("MSIE")) {
            return "IE";
        }elseif(self::chkBrowser("Firefox")){
            return "Firefox";
        }elseif(self::chkBrowser("Chrome")){
            return "Chrome";
        }elseif(self::chkBrowser("Safari")){
            return "Safari";
        }elseif(self::chkBrowser("Opera")){
            return "Opera";
        }elseif(self::chkBrowser("Netscape")){
            return "Netscape";
        }

        return "Unknown";
    }

    protected static function chkBrowser($nameBrowser) {
        return preg_match("/".$nameBrowser."/",$_SERVER['HTTP_USER_AGENT']);
    }
}


?>