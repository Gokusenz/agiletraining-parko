<!DOCTYPE html>
<html>
  <head>
    <title>
      Park - Ko
    </title>
    
    {{ HTML::style('vendors/seven/stylesheets/bootstrap.min.css') }}
    {{ HTML::style('vendors/seven/stylesheets/font-awesome.min.css') }}
    {{ HTML::style('vendors/seven/stylesheets/se7en-font.css') }}
    {{ HTML::style('vendors/seven/stylesheets/style.css') }}
    {{ HTML::style('vendors/seven/stylesheets/font-awesome.min.css') }}
    {{ HTML::style('vendors/seven/stylesheets/font-awesome.min.css') }}
    {{ HTML::style('css/style.css') }}
    


    <!--<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>-->
    {{ HTML::script('vendors/seven/javascripts/jquery.js'); }}
    {{ HTML::script('vendors/seven/javascripts/jquery_ujs.js'); }}
    {{ HTML::script('vendors/seven/javascripts/bootstrap.min.js'); }}
    
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body class="page-header-fixed bg-1 sidebar-nav">
    <div class="modal-shiftfix">
      
      <!-- Navigation -->
      <div class="navbar navbar-fixed-top scroll-hide" id="parko-navbar">
        <div class="container-fluid top-bar" id="parko-topbar">
          <div class="pull-right">
            <ul class="nav navbar-nav pull-right">
              
              <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img width="34" height="34" src="{{ asset('img/avatar-male.jpg') }}" />บุ๊งกี๋<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">
                    <i class="fa fa-user"></i>My Account</a>
                  </li>
                  <li><a href="#">
                    <i class="fa fa-gear"></i>Account Settings</a>
                  </li>
                  <li><a href="login1.html">
                    <i class="fa fa-sign-out"></i>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span>
            <span class="icon-bar"></span></button>
            <a class="parkko-logo" href="/">Park-Ko</a>
          
        </div>
        
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a href="/"><span aria-hidden="true" class="se7en-home"></span>Home</a>
              </li>
              <li>
                <a class="" id="ladyparking" href="/">
                <span aria-hidden="true" class="se7en-feed"></span>Lady Parking</a>
              </li>
              <li>
                <a class="" id="checkout" href="/checkout">
                <span aria-hidden="true" class="se7en-star"></span>Check out</a>
              </li>
             
            </ul>
          </div>
        </div>
        
      </div>
      <!-- End Navigation -->


      @yield('content')

    </div>
    {{ HTML::script('vendors/seven/javascripts/bootstrap.min.js'); }}
    {{ HTML::script('js/script.js'); }}
  </body>
</html>