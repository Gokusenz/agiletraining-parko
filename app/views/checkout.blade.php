@extends('master')

@section('content')
<script type="text/javascript">
  $('#checkout').addClass("current");
</script>
<div class="container-fluid main-content" style="margin-top:100px;">
  <h1 clas="page-title" style="text-align:center;">ชำระเงินออก</h1>
  <div class="row" style="margin-top:50px;">
    <div class="col-sm-6 text-center">
      <a href="/checkout-receipt?id=1" class="btn btn-primary btn-lg">สฬ 5420</a>
    </div>
    <div class="col-sm-6 text-center">
      <a href="/checkout-receipt?id=2" class="btn btn-primary btn-lg">ฌข 1597</a>
    </div>
  </div>
</div>
@stop