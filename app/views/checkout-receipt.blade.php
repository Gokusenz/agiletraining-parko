@extends('master')

@section('content')
<script type="text/javascript">
  $('#checkout').addClass("current");
</script>
<div class="container-fluid main-content" style="margin-top:100px;">
  <div class="row">
  	<div class="col-sm-5 col-sm-offset-4">
  		<div class="widget-container" style="margin-bottom:50px;">
  			<div class="widget-content padded">
  				<h1 style="text-align:center;">ใบเสร็จรับเงิน</h1>
  				<table class="table">
  					<tbody>
  						<tr>
  							<td>เลขทะเบียน</td>
  							<td>{{$rec->license}}</td>
  						</tr>
  						<tr>
  							<td>จังหวัด</td>
  							<td>{{$rec->province}}</td>
  						</tr>
  						<tr>
  							<td>รวมเวลาจอด</td>
  							<td>{{$time}} ชั่วโมง</td>
  						</tr>
  						<tr>
  							<td>ค่าจอด</td>
  							<td>{{$amounts}} บาท</td>
  						</tr>
  						<tr>
  							<td>รับเงิน</td>
  							<td>{{$rec->paid}} บาท</td>
  						</tr>
  						<tr>
  							<td>ทอนเงิน</td>
  							<td>{{$rec->paid - $amounts}} บาท</td>
  						</tr>
  						<tr>
  							<td>ทำรายการวันที่</td>
  							<td>{{thai_date(strtotime($timeout))}}</td>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  		</div>

  		<a href="#" class="btn btn-primary btn-lg btn-block" onclick="window.print();">PRINT</a>

  	</div>
  </div>
</div>
<?php
  function thai_date($time){
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
    $thai_month_arr=array(
      "0"=>"",
      "1"=>"มกราคม",
      "2"=>"กุมภาพันธ์",
      "3"=>"มีนาคม",
      "4"=>"เมษายน",
      "5"=>"พฤษภาคม",
      "6"=>"มิถุนายน",  
      "7"=>"กรกฎาคม",
      "8"=>"สิงหาคม",
      "9"=>"กันยายน",
      "10"=>"ตุลาคม",
      "11"=>"พฤศจิกายน",
      "12"=>"ธันวาคม"         
    );
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];
    $thai_date_return.= "ที่ ".date("j",$time);
    $thai_date_return.=" เดือน".$thai_month_arr[date("n",$time)];
    $thai_date_return.= " พ.ศ.".(date("Yํ",$time)+543);
    $thai_date_return.= "  ".date("H:i",$time)." น.";
    return $thai_date_return;
  }
?>
@stop