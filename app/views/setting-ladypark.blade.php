@extends('master')

@section('content')
<script type="text/javascript">
  $('#ladyparking').addClass("current");
</script>
<div class="container-fluid main-content">
        <div class="row">
          @if (isset($lady))
          @foreach($lady as $spot)
          <div class="col-sm-3">
            <a href="#" id="{{$spot->id}}" class="parking-spot {{ ($spot->status == 1) ? 'ladyparking' : ''; }}" 
            data-area="{{ $spot->id }}" 
            data-ladyparking="{{ $spot->status }}">{{ $spot->id; }}
              <span class="label-ladyparking">Lady Parking</span>
            </a>
          </div>
          @endforeach
		  @endif
</div>
@stop