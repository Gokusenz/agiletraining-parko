<?php

class ParkoController extends Controller {

  public function getIndex() {
    $ladys = Lady::all();
    $data = array(
      'lady' => $ladys,
    );
    return View::make('setting-ladypark', $data);
  }

  public function postIndex() {
    $id    = (int)Input::get('id');
    $status = Input::get('status');

    $lady = Lady::find($id);
      $lady['status'] = $status;
    $lady->save();
    // return "get id = ".$id." / status = ".$status;
  }

  public function getCheckout() {
    return View::make('checkout');
  }

  public function getCheckoutReceipt() {
    $id = (int)Input::get('id');
    $rec = Receipt::find($id);

    $time_out = date("Y-m-d H:i");
    $amounts = $this->calculateMoney($rec['time_in'], $time_out);
    $time_result = $this->getTimeDifference($rec['time_in'], $time_out);

    $data = array(
      'rec' => $rec,
      'timeout' => $time_out,
      'time' => $time_result,
      'amounts' => $amounts,
    );
    return View::make('checkout-receipt', $data);
  }


  // Test
  public function getSettingLadyparktest() {
    $id    = Input::get('id');
    $status = Input::get('status');

    if(Input::has('id')) {
      $lady = Lady::find($id);
      $lady['status'] = Input::get('status');
      $lady->save();
    }
  }


  function calculateMoney($dateTimeIn, $dateTimeOut) {

    //11:00 - 16:00 (+1)
    $hours = $this->getTimeDifference($dateTimeIn, $dateTimeOut); //29 = 24 + 5

    if ($hours < 1) {
      return 0;
    }
    if ($hours < 4) {
      return 10;
    }
    
    $offHour = $this->countOffHours($dateTimeIn, $dateTimeOut); //return 3.5

    $onHour = $hours - $offHour - 4; // 29 - 4 - 3.5 = 21.5
    // echo 'onHour = '.$onHour.'<br>';
    // echo 'offHour = '.$offHour.'<br>';
    
    return 10 + ceil($onHour) * 20 +  ceil($offHour) * 250;

  }

  function getTimeDifference($strDateIn, $strDateOut)
  {
    $dateTimeIn = new DateTime($strDateIn);
    $dateTimeOut = new DateTime($strDateOut);
    
    $interval = $dateTimeIn->diff($dateTimeOut);
    $hour = $interval->format('%h') + ($interval->days*24);
    $min = $interval->format('%i');

    return ($min > 0) ? $hour + 1 : $hour;  
  }

  function countOffHours($dateTimeIn, $dateTimeOut) 
  {
    $dateIn = strtotime($dateTimeIn);
    $dateInDate = date('d', $dateIn);
    $dateInMonth = date('m', $dateIn);
    $dateInYear = date('Y', $dateIn);
    
    // echo $dateInDate.'<br>';
    // echo $dateInMonth.'<br>';
    // echo $dateInYear.'<br>';
    
    //if (strtotime($dateTimeOut) > strtotime("Tomorrow 6am") ) { 
    if (strtotime($dateTimeOut) > mktime(6, 0, 0, $dateInMonth, $dateInDate + 1, $dateInYear) ) { 
      return 3.5;
    } else {
      return 0;
    }
  }

}