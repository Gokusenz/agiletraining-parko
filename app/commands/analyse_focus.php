<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class analyse_focus extends Command {

  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'analyse_focus';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Set analyse focus.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function fire()
  {
    // $this->line('Welcome to the SET Crawler.');

    $mode = "SET";
    $mode = $this->argument('mode');
        // $age = $this->option('age');
    // $this->line("GOKU SETWATCH : {$mode}.");


    // @ MODE : UPDATE
    // Select Stock for DB
    // $stock_list = DB::select('select * from sw_marketdetail where market = ? and status = ?', array("SET", 1));
    $stock_list = explode(",",$mode);

    // Input value
    for ($i=0; $i < count($stock_list); $i++) {
        // echo $stock_list[$i]."\n";
        $data[$i] = $this->crawl_page('http://marketdata.set.or.th/mkt/stockquotation.do?language=en&country=US', $stock_list[$i], false);
        echo $data[$i]['symbol']." ".$data[$i]['last']." ".$data[$i]['change']." ".$data[$i]['changeper']."%\n";
    }
  }

  /**
   * Get the console command arguments.
   *
   * @return array
   */
  protected function getArguments()
  {
    return array(
      array('mode', InputArgument::OPTIONAL, 'Mode of crawler', 'Update'),
      // array('example', InputArgument::REQUIRED, 'An example argument.'),
    );
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return array(
      array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
    );
  }

  // More function
  /**
   * Get HTML and remove spacial character.
   *
   * @return array
   */
  protected function compressHtml($html)
  {
      $output = str_replace(array("\r\n", "\r"), "\n", $html);
      $lines = explode("\n", $output);
      $new_lines = array();

      foreach ($lines as $i => $line) {
          if(!empty($line))
              $new_lines[] = trim($line);
      }
      $html = implode($new_lines);
      return $html;
  }

  /**
   * Crawl and get content.
   *
   * @return array
   */
  protected function crawl_page($url, $symbol, $saveStatus = false)
  {
      $postdata = http_build_query(
          array(
              'symbol' => $symbol
          )
      );

      $opts = array('http' =>
          array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/x-www-form-urlencoded',
              'content' => $postdata
          )
      );

      $context  = stream_context_create($opts);

      $result = file_get_contents($url , false, $context);

      $html = $this->compressHtml($result);

      // echo $html;

      // Symbol
      // preg_match('/<td class="topic" width="390">(.*?) :/',$html, $crawlResult);
      //     // print_r($updateResult);
      //     echo $crawlResult[1]."\n";
      $data['symbol'] = $symbol;

      // Last
      // Case : 1 -> +,-
      // Case : 2 -> =
      if (preg_match('/<font color=".*?><strong><img.*?>(.*?)<\/strong>/',$html, $nameResult)) {
          // echo $nameResult[1]."\n";
      } else {
          preg_match('/<font color=".*?><strong>(.*?)<\/strong>/',$html, $nameResult);
          // echo $nameResult[1]."\n";
      }
      if (isset($nameResult[1])) {
          $data['last'] = $nameResult[1];
      } else {
          $data['last'] = "Invalid";
      }

      // Change, Change %
      // Case : 1 -> +,-
      // Case : 2 -> =
      if (preg_match_all('/<font style=".*?>(.*?)</',$html, $changeResult)) {
          // echo $changeResult[1][0]."\n";
          // echo $changeResult[1][1]."%\n";
          $data['change'] = $changeResult[1][0];
          $data['changeper'] = $changeResult[1][1];
      } else {
          // echo "-\n-";
          $data['change'] = "-";
          $data['changeper'] = "-";
      }

      // Last Update
      preg_match('/Last Update (.*?)</',$html, $updateResult);
      if (isset($updateResult[1])) {
          $data['updatetime'] = $updateResult[1];
          $data['status'] = 1;
      } else {
          $data['updatetime'] = "Invalid";
          $data['status'] = 0;
      }

      return $data;
  }

  protected function crawl_namestock($url)
  {
    $stock = array();
      $result = file_get_contents($url);

      $html = $this->compressHtml($result);

      // Name Stock
      preg_match_all('/<td><a href="\/set\/companyprofile.do.*?>(.*?)<\/a><\/td><td>(.*?)<\/td><td align="center">(.*?)<\/td>/',$html, $crawlResult);

          for ($i=0; $i < count($crawlResult[1]); $i++) {
              $stock[$i]['name'] = $crawlResult[1][$i];
              $stock[$i]['decription'] = $crawlResult[2][$i];
              $stock[$i]['market'] = $crawlResult[3][$i];
          }
          // print_r($stock);
      return $stock;
  }

}
