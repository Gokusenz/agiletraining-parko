<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class analyse_set extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'analyse';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set analyse.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->line('Welcome to the SET Crawler.');

		$mode = $this->argument('mode');
        // $age = $this->option('age');
		$this->line("Mode : {$mode}.");

		// Reset status of stock
		if($mode == 'reset') {
			$reset_rows = DB::update('update sw_marketdetail set status = ? where status = ?', array(1, 0));
		    printf("%d Row reset.\n", $reset_rows);
			exit();
		} else if($mode == 'namestock') {
			$letter = range('A','Z');
			for ($z=0; $z < count($letter); $z++) {
			    echo $letter[$z]."\n";
			    $stock_list[$z] = $this->crawl_namestock('http://www.set.or.th/set/commonslookup.do?prefix='.$letter[$z].'&language=en&country=US');
			}

			$insertCount =0;
			for ($i=0; $i < count($stock_list); $i++) {
			    for ($y=0; $y < count($stock_list[$i]); $y++) {
			        $stock_list[$i][$y]['name'] = str_replace('&amp;', '&', $stock_list[$i][$y]['name']);
			        $checkResult = DB::select('select * from sw_marketdetail where symbol = ?', array($stock_list[$i][$y]['name']));
					if($checkResult == 0) {
						echo "Insert : ".$data[$y]['symbol']."\n";
						$insert = DB::insert('insert into sw_marketdetail (id, name, symbol, market) values (?, ?)', array(NULL, $stock_list[$i][$y]['decription'], $stock_list[$i][$y]['name'], $stock_list[$i][$y]['market']));
				        $insertCount = $insertCount + $insert;
					}
			    }
			}
	        printf("%d Row inserted.\n", $insertCount);
			exit();
		}

		// @ MODE : UPDATE
		// Select Stock for DB
		$stock_list = DB::select('select * from sw_marketdetail where market = ? and status = ?', array("SET", 1));

		// Input value
		for ($i=0; $i < count($stock_list); $i++) {
		    echo $stock_list[$i]->symbol."\n";
		    $data[$i] = $this->crawl_page('http://marketdata.set.or.th/mkt/stockquotation.do?language=en&country=US', $stock_list[$i]->symbol, false);
		}

		// UPDATE DATA
		$updateCount = 0;
		for ($y=0; $y < count($data); $y++) {
		    // echo $data[$y]['symbol']." : ";
			$checkResult = DB::select('select * from sw_stockdata where symbol = ?', array($data[$y]['symbol']));
			if($checkResult == 0) {
				echo "Insert : ".$data[$y]['symbol']."\n";
		        $insert = DB::insert('insert into sw_stockdata (id, symbol, last, changes, changesper, updatetime, status) values (?, ?, ?, ?, ?, ?, ?)', array(NULL, $data[$y]['symbol'], $data[$y]['last'], $data[$y]['change'], $data[$y]['changeper'], $data[$y]['updatetime'], $data[$y]['status']));
			} else {
				$update_rows = DB::update('update sw_stockdata set last = ?, changes = ?, changesper = ?, updatetime = ?, status = ? where symbol = ?', array($data[$y]['last'], $data[$y]['change'], $data[$y]['changeper'], $data[$y]['updatetime'], $data[$y]['status'], $data[$y]['symbol']));
			    $updateCount = $updateCount + $update_rows;
			}
		}
	    printf("%d Row updated.\n", $updateCount);


		// UPDATE STOCK STATUS
		echo "########## Start update status to stock_list ############### \n";
		$statusList = DB::select('select symbol,status from sw_stockdata', array());

		// print_r($statusList);
		$updateCount = 0;
		for ($i=0; $i < count($statusList); $i++) {
			$update_rows = DB::update('update sw_marketdetail set status = ? where symbol = ?', array($statusList[$i]->status, $statusList[$i]->symbol));
		    $updateCount = $updateCount + $update_rows;
		}
	    printf("%d Row updated.\n", $updateCount);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('mode', InputArgument::OPTIONAL, 'Mode of crawler', 'Update'),
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

	// More function
	/**
	 * Get HTML and remove spacial character.
	 *
	 * @return array
	 */
	protected function compressHtml($html)
	{
	    $output = str_replace(array("\r\n", "\r"), "\n", $html);
	    $lines = explode("\n", $output);
	    $new_lines = array();

	    foreach ($lines as $i => $line) {
	        if(!empty($line))
	            $new_lines[] = trim($line);
	    }
	    $html = implode($new_lines);
	    return $html;
	}

	/**
	 * Crawl and get content.
	 *
	 * @return array
	 */
	protected function crawl_page($url, $symbol, $saveStatus = false)
	{
	    $postdata = http_build_query(
	        array(
	            'symbol' => $symbol
	        )
	    );

	    $opts = array('http' =>
	        array(
	            'method'  => 'POST',
	            'header'  => 'Content-type: application/x-www-form-urlencoded',
	            'content' => $postdata
	        )
	    );

	    $context  = stream_context_create($opts);

	    $result = file_get_contents($url , false, $context);

	    $html = $this->compressHtml($result);

	    // echo $html;

	    // Symbol
	    // preg_match('/<td class="topic" width="390">(.*?) :/',$html, $crawlResult);
	    //     // print_r($updateResult);
	    //     echo $crawlResult[1]."\n";
	    $data['symbol'] = $symbol;

	    // Last
	    // Case : 1 -> +,-
	    // Case : 2 -> =
	    if (preg_match('/<font color=".*?><strong><img.*?>(.*?)<\/strong>/',$html, $nameResult)) {
	        // echo $nameResult[1]."\n";
	    } else {
	        preg_match('/<font color=".*?><strong>(.*?)<\/strong>/',$html, $nameResult);
	        // echo $nameResult[1]."\n";
	    }
	    if (isset($nameResult[1])) {
	        $data['last'] = $nameResult[1];
	    } else {
	        $data['last'] = "Invalid";
	    }

	    // Change, Change %
	    // Case : 1 -> +,-
	    // Case : 2 -> =
	    if (preg_match_all('/<font style=".*?>(.*?)</',$html, $changeResult)) {
	        // echo $changeResult[1][0]."\n";
	        // echo $changeResult[1][1]."%\n";
	        $data['change'] = $changeResult[1][0];
	        $data['changeper'] = $changeResult[1][1];
	    } else {
	        // echo "-\n-";
	        $data['change'] = "-";
	        $data['changeper'] = "-";
	    }

	    // Last Update
	    preg_match('/Last Update (.*?)</',$html, $updateResult);
	    if (isset($updateResult[1])) {
	        $data['updatetime'] = $updateResult[1];
	        $data['status'] = 1;
	    } else {
	        $data['updatetime'] = "Invalid";
	        $data['status'] = 0;
	    }

	    return $data;
	}

	protected function crawl_namestock($url)
	{
		$stock = array();
	    $result = file_get_contents($url);

	    $html = $this->compressHtml($result);

	    // Name Stock
	    preg_match_all('/<td><a href="\/set\/companyprofile.do.*?>(.*?)<\/a><\/td><td>(.*?)<\/td><td align="center">(.*?)<\/td>/',$html, $crawlResult);

	        for ($i=0; $i < count($crawlResult[1]); $i++) {
	            $stock[$i]['name'] = $crawlResult[1][$i];
	            $stock[$i]['decription'] = $crawlResult[2][$i];
	            $stock[$i]['market'] = $crawlResult[3][$i];
	        }
	        // print_r($stock);
	    return $stock;
	}

}
